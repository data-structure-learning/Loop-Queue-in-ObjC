//
//  LoopQueue.h
//  Loop-Queue-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoopQueue : NSObject {
@private
    int *m_pQueue;
    int m_iQueueLen;
    int m_iQueueCapacity;
    
    int m_iHead;
    int m_iTail;
}

- (instancetype)initWithQueueCapacity:(int)queueCapacity;
- (void)ClearQueue;
- (BOOL)QueueEmpty;
- (BOOL)QueueFull;
- (int)QueueLength;
- (BOOL)EnQueue:(int)element;
- (BOOL)DeQueue:(int *)element;
- (void)QueueTraverse;

@end
