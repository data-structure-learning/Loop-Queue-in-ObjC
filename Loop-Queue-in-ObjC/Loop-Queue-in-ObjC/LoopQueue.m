//
//  LoopQueue.m
//  Loop-Queue-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import "LoopQueue.h"

@implementation LoopQueue

- (instancetype)initWithQueueCapacity:(int)queueCapacity {
    self = [super init];
    if (self) {
        m_iQueueCapacity = queueCapacity;
        m_pQueue = (int *)malloc(sizeof(int)*queueCapacity);
        [self ClearQueue];
    }
    return self;
}

- (void)ClearQueue {
    m_iHead = 0;
    m_iTail = 0;
    m_iQueueLen = 0;
}

- (BOOL)QueueEmpty {
    return m_iQueueLen == 0;
}

- (BOOL)QueueFull {
    return m_iQueueLen == m_iQueueCapacity;
}

- (int)QueueLength {
    return m_iQueueLen;
}

- (BOOL)EnQueue:(int)element {
    if ([self QueueFull]) {
        return NO;
    }
    
    m_pQueue[m_iTail] = element;
    m_iQueueLen += 1;
    m_iTail += 1;
    m_iTail %= m_iQueueCapacity;
    return YES;
}

- (BOOL)DeQueue:(int *)element {
    if ([self QueueEmpty]) {
        return NO;
    }
    
    *element = m_pQueue[m_iHead];
    m_iQueueLen -= 1;
    m_iHead += 1;
    m_iHead %= m_iQueueCapacity;
    return YES;
}

- (void)QueueTraverse {
    for (int i = m_iHead; i < m_iHead + m_iQueueLen; i ++) {
        NSLog(@"%d", m_pQueue[i%m_iQueueLen]);
    }
    NSLog(@"\n");
}

@end
