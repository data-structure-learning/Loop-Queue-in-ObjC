//
//  main.m
//  Loop-Queue-in-ObjC
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoopQueue.h"

void testLoopQueue();

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSLog(@"testLoopQueue");
        testLoopQueue();
    }
    return 0;
}

void testLoopQueue() {
    LoopQueue *q = [[LoopQueue alloc] initWithQueueCapacity:5];
    
    [q EnQueue:2];
    [q EnQueue:5];
    
    [q QueueTraverse];
    
    int e = 0;
    [q DeQueue:&e];
    NSLog(@"e: %d\n", e);
    
    [q EnQueue:7];
    [q EnQueue:12];
    [q EnQueue:19];
    [q EnQueue:31];
    [q EnQueue:50];
    
    [q QueueTraverse];
    
    [q ClearQueue];
    
    [q QueueTraverse];

}
